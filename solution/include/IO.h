#ifndef IMAGE_TRANSFORMER_IO_H
#define IMAGE_TRANSFORMER_IO_H

#include <stdbool.h>
#include <stdio.h>

struct result {
    FILE *file;
    bool is_valid;
};

struct result *open(char *name, char *mode);

void close(struct result *result);


#endif //IMAGE_TRANSFORMER_IO_H
