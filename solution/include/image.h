#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdint.h>

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

void create_image(struct image *img, uint64_t width, uint64_t height);

void free_up_memory(struct image *img);

#endif //IMAGE_TRANSFORMER_IMAGE_H
