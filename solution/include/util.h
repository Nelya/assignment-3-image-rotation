#include "bmp.h"
#include <stdint.h>
#include <stdio.h>

#ifndef IMAGE_TRANSFORMER_UTIL_H
#define IMAGE_TRANSFORMER_UTIL_H


enum read_status check_valid_read_type(FILE *in);

enum read_status check_valid_read_data(FILE *in, struct image *img);

enum write_status check_valid_write(const struct image *img);

enum write_status write_header(FILE *out, uint64_t width, uint64_t height);

void write_padding(FILE *out, uint64_t width);

#endif //IMAGE_TRANSFORMER_UTIL_H
