#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H

struct image rotate(struct image *source, int angle);

#endif //IMAGE_TRANSFORMER_ROTATION_H
