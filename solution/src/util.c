#define TYPE 0x4D42
#define BFRESERVED 0
#define BOFFBITS 54
#define BISIZE 40
#define BIPLANES 1
#define BIBITCOUNT 24
#define BICOMPRESSION 0
#define BISIZEIMAGE 0
#define BIXPELSPERMETER 0
#define BIYPELSPERMETER 0
#define BICLRUSED 0
#define BICLRIMPORTANT 0

#include "bmp.h"
#include "util.h"

enum read_status check_valid_read_type(FILE *in) {
    uint16_t checker;
    if (!in) {
        return INVALID_REFERENCE;
    }
    if (fread(&checker, sizeof(uint16_t), 1, in) != 1 || checker != TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    fseek(in, 18, SEEK_SET);
    return READ_OK;
}

enum read_status check_valid_read_data(FILE *in, struct image *img) {
    size_t row_size = (img->width * sizeof(struct pixel) + 3) / 4 * 4;
    for (uint64_t i = 0; i < img->height; ++i) {
        if (fread(img->data + i * img->width, sizeof(struct pixel), img->width, in) != img->width) {
            return READ_INVALID_BITS;
        }
        fseek(in, (long) (row_size - img->width * sizeof(struct pixel)), SEEK_CUR);
    }
    return READ_OK;
}

enum write_status check_valid_write(const struct image *img) {
    if (img->data == NULL) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum write_status write_to_file(const void *str, size_t size, size_t count, FILE *file) {
    if (fwrite(str, size, count, file) != count) {
        return WRITE_ERROR_HEADER;
    }
    return WRITE_OK;
}

enum write_status write_header(FILE *out, uint64_t width, uint64_t height) {
    uint16_t type = TYPE, biPlanes = BIPLANES, biBitCount = BIBITCOUNT;
    uint32_t bfReserved = BFRESERVED, bOffBits = BOFFBITS, biSize = BISIZE, biCompression = BICOMPRESSION, biSizeImage = BISIZEIMAGE, biXPelsPerMeter = BIXPELSPERMETER, biYPelsPerMeter = BIYPELSPERMETER, biClrUsed = BICLRUSED, biClrImportant = BICLRIMPORTANT;
    uint32_t bfileSize = width * height * sizeof(struct pixel) + BOFFBITS;
    if (write_to_file(&type, sizeof(uint16_t), 1, out) ||
        write_to_file(&bfileSize, sizeof(uint32_t), 1, out) ||
        write_to_file(&bfReserved, sizeof(uint32_t), 1, out) ||
        write_to_file(&bOffBits, sizeof(uint32_t), 1, out) ||
        write_to_file(&biSize, sizeof(uint32_t), 1, out) ||
        write_to_file(&width, sizeof(uint32_t), 1, out) ||
        write_to_file(&height, sizeof(uint32_t), 1, out) ||
        write_to_file(&biPlanes, sizeof(uint16_t), 1, out) ||
        write_to_file(&biBitCount, sizeof(uint16_t), 1, out) ||
        write_to_file(&biCompression, sizeof(uint32_t), 1, out) ||
        write_to_file(&biSizeImage, sizeof(uint32_t), 1, out) ||
        write_to_file(&biXPelsPerMeter, sizeof(uint32_t), 1, out) ||
        write_to_file(&biYPelsPerMeter, sizeof(uint32_t), 1, out) ||
        write_to_file(&biClrUsed, sizeof(uint32_t), 1, out) ||
        write_to_file(&biClrImportant, sizeof(uint32_t), 1, out)) {
        return WRITE_ERROR_HEADER;
    }
    return WRITE_OK;
}

void write_padding(FILE *out, uint64_t width) {
    size_t padding_size = (4 - (width * sizeof(struct pixel)) % 4) % 4;
    uint8_t padding[4] = {0};
    fwrite(padding, sizeof(uint8_t), padding_size, out);
}
