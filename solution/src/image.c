#include "image.h"
#include <malloc.h>

void create_image(struct image *img, uint64_t width, uint64_t height) {
    img->data = malloc(width * height * sizeof(struct pixel));
    if (!img->data) {
        return;
    }
    img->width = width;
    img->height = height;

}

void free_up_memory(struct image *img) {
    if (img != NULL && img->data != NULL) {
        free(img->data);
    }
}
