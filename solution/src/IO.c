#include "IO.h"
#include <malloc.h>
#include <stdbool.h>

static struct result EMPTY = {0};

static struct result *create(FILE *file) {
    struct result *a = malloc(sizeof(struct result));
    if (!a) {
        return &EMPTY;
    }
    a->file = file;
    a->is_valid = true;
    return a;
}

struct result *open(char *name, char *mode) {
    FILE *in = fopen(name, mode);
    if (!in) {
        return &EMPTY;
    }
    return create(in);
}

void close(struct result *result) {
    if (!result->file) {
        return;
    }
    fclose(result->file);
    free(result);
}



