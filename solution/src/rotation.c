#include "image.h"
#include "rotation.h"
#include "util.h"

static struct image rotate_90_degrees(struct image *source, struct image output_img);

static struct image rotate_180_degrees(struct image *source, struct image output_img);

static struct image rotate_270_degrees(struct image *source, struct image output_img);

static struct image not_rotate(struct image *source, struct image output_img);

struct image rotate(struct image *source, int angle) {
    struct image output_img;
    uint64_t output_width = source->width;
    uint64_t output_height = source->height;
    create_image(&output_img, output_width, output_height);
    if (angle == 90 || angle == -270) {
        output_img = rotate_90_degrees(source, output_img);;
    } else if (angle == 180 || angle == -180) {
        rotate_180_degrees(source, output_img);
    } else if (angle == 270 || angle == -90) {
        output_img = rotate_270_degrees(source, output_img);
    } else {
        not_rotate(source, output_img);
    }
    return output_img;
}

static struct image rotate_90_degrees(struct image *source, struct image output_img) {
    output_img.width = source->height;
    output_img.height = source->width;
    uint64_t new_x, new_y;
    for (uint64_t x = 0; x < (source->width); x++) {
        for (uint64_t y = 0; y < (source->height); y++) {
            new_x = y;
            new_y = source->width - x - 1;
            if (new_x < output_img.width && new_y < output_img.height) {
                output_img.data[output_img.width * new_y + new_x] = source->data[source->width * y + x];
            }
        }
    }
    return output_img;
}

static struct image rotate_180_degrees(struct image *source, struct image output_img) {
    uint64_t new_x, new_y;
    for (uint64_t x = 0; x < (source->width); x++) {
        for (uint64_t y = 0; y < (source->height); y++) {
            new_x = source->width - x - 1;
            new_y = source->height - y - 1;
            if (new_x < output_img.width && new_y < output_img.height) {
                output_img.data[output_img.width * new_y + new_x] = source->data[source->width * y + x];
            }
        }
    }
    return output_img;
}

static struct image rotate_270_degrees(struct image *source, struct image output_img) {
    output_img.width = source->height;
    output_img.height = source->width;
    uint64_t new_x, new_y;
    for (uint64_t x = 0; x < (source->width); x++) {
        for (uint64_t y = 0; y < (source->height); y++) {
            new_x = source->height - y - 1;
            new_y = x;
            if (new_x < output_img.width && new_y < output_img.height) {
                output_img.data[output_img.width * new_y + new_x] = source->data[source->width * y + x];
            }
        }
    }
    return output_img;
}

static struct image not_rotate(struct image *source, struct image output_img) {
    uint64_t new_x, new_y;
    for (uint64_t x = 0; x < (source->width); x++) {
        for (uint64_t y = 0; y < (source->height); y++) {
            new_x = x;
            new_y = y;
            if (new_x < output_img.width && new_y < output_img.height) {
                output_img.data[output_img.width * new_y + new_x] = source->data[source->width * y + x];
            }
        }
    }
    return output_img;
}
