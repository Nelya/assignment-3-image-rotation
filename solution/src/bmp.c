#define TYPE 0x4D42
#define BOFFBITS 54

#include "bmp.h"
#include "image.h"
#include "util.h"

enum read_status from_bmp(FILE *in, struct image *img) {
    enum read_status status1 = check_valid_read_type(in);
    if (status1 != READ_OK) {
        return status1;
    }
    uint32_t width = 0, height = 0;
    size_t read_count = fread(&width, sizeof(uint32_t), 1, in);
    if (read_count != 1) {
        return READ_INVALID_BITS;
    }
    read_count = fread(&height, sizeof(uint32_t), 1, in);
    if (read_count != 1) {
        return READ_INVALID_BITS;
    }
    fseek(in, BOFFBITS, SEEK_SET);
    create_image(img, width, height);
    enum read_status status2 = check_valid_read_data(in, img);
    if (status2 != READ_OK) {
        return status2;
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, const struct image *img) {
    enum write_status status1 = check_valid_write(img);
    if (status1 != WRITE_OK) {
        return WRITE_ERROR;
    }
    enum write_status status2 = write_header(out, img->width, img->height);
    if (status2 != WRITE_OK) {
        return WRITE_ERROR;
    }
    for (uint64_t i = 0; i < img->height; ++i) {
        fwrite((img->width) * i + img->data, sizeof(struct pixel), img->width, out);
        write_padding(out, img->width);
    }
    return WRITE_OK;
}







