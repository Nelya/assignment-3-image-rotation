#include "bmp.h"
#include "IO.h"
#include "image.h"
#include "rotation.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc != 4) {
        for (int i = 0; i < argc; i++) {
            perror("must be 4 arguments");
        }
        return 1;
    }
    int angle = atoi(argv[3]);
    if (!(angle > -360 && angle < 360 && angle % 90 == 0)) {
        perror("angle can only be the following values: 0, +-90, +-180, +-270");
        return 1;
    }
    struct result *result = open(argv[1], "rb");
    if (!result->is_valid) {
        close(result);
        perror("can't open input file");
        return 1;
    }

    struct image input_image;
    if (from_bmp(result->file, &input_image) != READ_OK) {
        close(result);
        perror("error reading from file");
        free_up_memory(&input_image);
        return 1;
    }
    struct result *result_out = open(argv[2], "wb");
    if (!result_out->is_valid) {
        close(result_out);
        perror("can't open output file");
        return 1;
    }
    struct image img = rotate(&input_image, angle);
    if (to_bmp(result_out->file, &img) != WRITE_OK) {
        close(result_out);
        perror("error writing to file");
        free_up_memory(&input_image);
        free_up_memory(&img);
        return 1;
    }
    free_up_memory(&input_image);
    free_up_memory(&img);
    close(result);
    close(result_out);
    return 0;
}
